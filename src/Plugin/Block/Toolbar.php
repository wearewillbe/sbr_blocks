<?php
/**
 * @file
 * Contains \Drupal\sbr_blocks\Plugin\Block\Toolbar.
 */
namespace Drupal\sbr_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Toolbar' Block.
 *
 * @Block(
 *   id = "toolbar",
 *   admin_label = @Translation("Toolbar"),
 *   category = @Translation("SBR"),
 * )
 */
class Toolbar extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#markup' => $this->t('A custom Toolbar Block'),
    );
  }

}
