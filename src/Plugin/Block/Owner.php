<?php
/**
 * @file
 * Contains \Drupal\sbr_blocks\Plugin\Block\Owner.
 */
namespace Drupal\sbr_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Owner' Block.
 *
 * @Block(
 *   id = "Owner",
 *   admin_label = @Translation("Owner"),
 *   category = @Translation("SBR"),
 * )
 */
class Owner extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#markup' => $this->t('A custom Owner Block'),
    );
  }

}
