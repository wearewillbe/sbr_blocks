<?php
/**
 * @file
 * Contains \Drupal\sbr_blocks\Plugin\Block\Imprint.
 */
namespace Drupal\sbr_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Imprint' Block.
 *
 * @Block(
 *   id = "imprint",
 *   admin_label = @Translation("Imprint"),
 *   category = @Translation("SBR"),
 * )
 */
class Imprint extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#markup' => $this->t('A custom Imprint Block'),
    );
  }

}
